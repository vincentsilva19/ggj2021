﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class CharactersAnimations : MonoBehaviour
{
    public Animator animator;
    public NavMeshAgent agent;
    public Renderer[] renderers;

    MaterialPropertyBlock propertyBlock;
    public void Awake()
    {
        propertyBlock = new MaterialPropertyBlock();
        renderers = GetComponentsInChildren<Renderer>();
    }

    public void Update()
    {
        if (agent.remainingDistance < .1f)
            Stand();
        else
            Walk();
    }

    public void Walk() {
        animator.SetBool("walking", true);
    }

    // Update is called once per frame
    public void Stand()
    {
        animator.SetBool("walking", false);
    }

    public void ShowOutline()
    {
        foreach (var render in renderers)
        {
            render.GetPropertyBlock(propertyBlock);
            propertyBlock.SetFloat("Outline", 1);
            render.SetPropertyBlock(propertyBlock);
        }
    }

    public void HideOutline()
    {
        foreach (var render in renderers)
        {
            render.GetPropertyBlock(propertyBlock);
            propertyBlock.SetFloat("Outline", 0);
            render.SetPropertyBlock(propertyBlock);
        }
    }
}
