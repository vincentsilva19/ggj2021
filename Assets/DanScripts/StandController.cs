﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandController : MonoBehaviour
{
    public Renderer[] renderers;
    public Texture[] textures;
    public Texture[] productTextures;
    public ColorPickerController textureController;
    public ColorPickerController productstextureController;
    public ColorPickerColors[] colorVariations;
    public ColorPickerColors[] productColorVariations;

    float targetAlpha = 1;
    float currentAlpha = 1;

    void Start()
    {
        SetAlpha(1);

        SetTexture(textureController, textures[Random.Range(0,textures.Length)]);
        textureController.SetColors(colorVariations[Random.Range(0, colorVariations.Length)]);
        
        SetTexture(productstextureController, productTextures[Random.Range(0, productTextures.Length)]);
        productstextureController.SetColors(colorVariations[Random.Range(0, colorVariations.Length)]);
    }

    // Update is called once per frame
    void Update()
    {
        currentAlpha = Mathf.Lerp(currentAlpha, targetAlpha, Time.deltaTime *4 );
        SetAlpha(currentAlpha);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Character"))
            targetAlpha = 0;
    }

    public void SetAlpha(float alpha)
    {
        foreach (var render in renderers)
        {
            textureController.SetAlpha(alpha);
        }
    }

    public void SetTexture(ColorPickerController contorller,Texture texture)
    {
        foreach (var render in renderers)
        {
            contorller.SetTexture(texture);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Character"))
            targetAlpha = 1;

    }

}
