﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPickerController : MonoBehaviour
{
    public Renderer[] renderers;

    public Color RedSubstitute;
    public Color GreenSubstitute;
    public Color BlueSubstitute;

    MaterialPropertyBlock propertyBlock;

    private void Awake()
    {
        propertyBlock = new MaterialPropertyBlock();
        SetColors(RedSubstitute, GreenSubstitute, BlueSubstitute);

    }
    public void SetTexture(Texture texture)
    {
        foreach (var render in renderers)
        {
            render.GetPropertyBlock(propertyBlock);
            propertyBlock.SetTexture("Diff", texture);
            render.SetPropertyBlock(propertyBlock);
        }
    }

    internal void SetColors(ColorPickerColors color)
    {
        SetColor("RedSub", color.R);
        SetColor("GreenSub", color.G);
        SetColor("BlueSub", color.B);
    }

    public void SetColors(Color r, Color g, Color b)
    {
        SetColor("RedSub", r);
        SetColor("GreenSub", g);
        SetColor("BlueSub", b);

    }

    public void SetAlpha(float alpha)
    {
        foreach (var render in renderers)
        {
            render.GetPropertyBlock(propertyBlock);
            propertyBlock.SetFloat("AlphaValue", alpha);
            render.SetPropertyBlock(propertyBlock);
        }
    }
    void SetColor(string shaderProperty, Color color)
    {
        foreach (var render in renderers)
        {
            render.GetPropertyBlock(propertyBlock);
            propertyBlock.SetColor(shaderProperty, color);
            render.SetPropertyBlock(propertyBlock);
        }
    }
}

[Serializable]
public class ColorPickerColors
{
    public Color R;
    public Color G;
    public Color B;
}
