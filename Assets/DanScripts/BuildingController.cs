﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
    public ColorPickerController colorController;
    public ColorPickerColors[] variations;
    void Start()
    {
        colorController.SetColors(variations[Random.Range(0, variations.Length)]);
    }
}

