﻿using System;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    [SerializeField] Transform focus = default;

    [SerializeField, Range(1f, 20f)] float distance = 5f;
    
    [SerializeField, Min(0f)] float focusRadius = 1f;

    [SerializeField, Range(0f, 1f)] float focusCentering = 0.5f;

    [SerializeField, Range(0f, 20f)] float rotationSensitivity = 10;
    
    Vector3 focusPoint;
    
    
    void Awake () {
        focusPoint = focus.position;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    
    void LateUpdate () {
        UpdateFocusPoint();
        var lookDirection = transform.forward;
        transform.localPosition = focusPoint - lookDirection * distance;
        float rotateHorizontal = Input.GetAxis ("Mouse X");
        // float rotateVertical = Input.GetAxis ("Mouse Y");
        
        // transform.RotateAround(focusPoint, Vector3.forward, rotateVertical * rotationSensitivity);
        transform.RotateAround (focusPoint, Vector3.up, rotateHorizontal * rotationSensitivity);
        focus.RotateAround(focusPoint, Vector3.up, rotateHorizontal * rotationSensitivity);
        
    }
    
    void UpdateFocusPoint () {
        var targetPoint = focus.position;
        if (focusRadius > 0f) {
            var distance = Vector3.Distance(targetPoint, focusPoint);
            var t = 1f;
            if (distance > 0.01f && focusCentering > 0f) {
                t = Mathf.Pow(1f - focusCentering, Time.deltaTime);
            }
            if (distance > focusRadius) {
                t = Mathf.Min(t, focusRadius / distance);
            }
            focusPoint = Vector3.Lerp(targetPoint, focusPoint, t);
        }
        else {
            focusPoint = targetPoint;
        }
    }
}
