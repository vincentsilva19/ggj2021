﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CallController : MonoBehaviour
{
    public AudioSource[] sources;
    [SerializeField, Range(0f, 10f)] public float audioFrecuency = 3f;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        PlaySoundAtRandom();
    }
    
    void PlaySoundAtRandom()
    {
        var random = Random.Range(0f, 10f);
        var pick = Random.Range(0, sources.Length);
        if (!sources.Any(source => source.isPlaying) && random < audioFrecuency * Time.deltaTime) 
            sources[pick].Play();
    }
}
