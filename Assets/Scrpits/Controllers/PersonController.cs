﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class PersonController : MonoBehaviour
{

    public CharactersAnimations characterMeshController;
    public Material selected;
    public Material iddle;
    public MeshRenderer meshRenderer;
    public bool canEnter;
    
    public float wanderRadius = 10f;
    public float wanderTimer = 3f;

    Transform target;
    NavMeshAgent agent;
    float timer;
    bool isMarked;
    
    void OnTriggerExit(Collider other)
    {
        meshRenderer.material = iddle;
    }


    public void Mark()
    {
        isMarked = true;
        characterMeshController.ShowOutline();
    }

    public void UnMark()
    {
        isMarked = false;
        characterMeshController.HideOutline();
        agent.SetDestination(transform.position);
    }
    
    void OnEnable () {
        agent = GetComponent<NavMeshAgent> ();
        timer = wanderTimer;
    }
    
    void Update () {
        timer += Time.deltaTime;
        if (timer >= wanderTimer &&  !isMarked) MoveRandom();
    }

    void MoveRandom()
    {
        Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
        agent.SetDestination(newPos);
        timer = 0;
    }

    static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
        var randDirection = Random.insideUnitSphere * dist;
 
        randDirection += origin;
 
        NavMeshHit navHit;
        NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);
 
        return navHit.position;
    }
}
