﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Scenes.VincentSceneAssets.Scripts;
using Scenes.VincentSceneAssets.Scripts.Domain;
using UnityEngine;

public class SelectionController : MonoBehaviour
{

    public GameEvents events;
    bool hasTarget => targets.Count > 0;
    List<PersonController> targets;
    float targetDistance;

    void Start()
    {
        targets = new List<PersonController>();
    }

    void Update()
    {
        if ((Input.GetKeyUp("f") || Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0)) && hasTarget)
        {
            if (targets[0].canEnter)
                events.CollectBodyCorrectBody();
            else
                events.CollectWrongBody();
        }
            
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Person"))
        {
            targets.Add(other.GetComponent<PersonController>());
            MarkFirst();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Person"))
        {
            targets.Remove(other.GetComponent<PersonController>());
            other.GetComponent<PersonController>().UnMark();
            MarkFirst();
        }
    }

    void MarkFirst()
    {
        targets.ForEach(target => target.UnMark());
        if (hasTarget)
            targets[0].Mark();
    }
}
