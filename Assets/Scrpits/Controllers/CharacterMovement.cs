﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scenes.VincentSceneAssets.Scripts.Controllers
{
    public class CharacterMovement : MonoBehaviour
    {
        public AudioSource[] sources;
        [SerializeField, Range(0f, 10f)] public float audioFrecuency = 3f;
        public ModelSway model;
        Vector3 playerVelocity;
        bool groundedPlayer;
        public float playerSpeed = 2.0f;
        float jumpHeight = 1.0f;
        float gravityValue = -9.81f;
        
        
        void Update()
        {
            var axisH = Input.GetAxisRaw("Horizontal");
            var axisV = Input.GetAxisRaw("Vertical");
            
            // var isMoving = Math.Abs(Math.Round(axisH, 2)) > 0 || Math.Abs(Math.Round(axisV, 2)) > 0;
            var isMoving = axisH != 0 || axisV != 0;
            model.isMoving = isMoving;
            Debug.Log(isMoving);
            // Debug.Log(axisH + " " + axisV + " " + Math.Abs(axisH) + " " + Math.Abs(axisV) + " " + isMoving);
            if (isMoving) PlaySoundAtRandom();

            var direction = new Vector3(axisH, 0, axisV);
            transform.Translate(direction * Time.deltaTime * playerSpeed);
        }

        void PlaySoundAtRandom()
        {
            var random = Random.Range(0f, 10f);
            var pick = Random.Range(0, sources.Length);
            if (!sources.Any(source => source.isPlaying) && random < audioFrecuency * Time.deltaTime) 
                sources[pick].Play();
        }
    }
}
