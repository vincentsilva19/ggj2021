﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class PersonSpawner : MonoBehaviour
{

    public GameObject person;
    public GameObject distinct;
    public GameObject target;
    
    int population = 5;
    int distinctPopulation = 200;

    List<Vector3> points = new List<Vector3>();
    void Start()
    {
        foreach (var variable in Enumerable.Range(1, population)) 
            InstantiatePerson(person);
        foreach (var variable in Enumerable.Range(1, distinctPopulation)) 
            InstantiatePerson(distinct);
        InstantiatePerson(target);
    }

    void InstantiatePerson(GameObject original)
    {
        var position = PickRandomPoint();
        var newBorn = Instantiate(original, position+Vector3.up, Quaternion.identity);
        newBorn.transform.parent = transform;
    }

    Vector3 PickRandomPoint()
    {
        var navMeshData = NavMesh.CalculateTriangulation();
 
        var maxIndices = navMeshData.indices.Length - 3;
        
        var firstVertexSelected = Random.Range(0, maxIndices);
        var secondVertexSelected = Random.Range(0, maxIndices);
 
        
        var result = navMeshData.vertices[navMeshData.indices[firstVertexSelected]];
 
        var firstVertexPosition = navMeshData.vertices[navMeshData.indices[firstVertexSelected]];
        var secondVertexPosition = navMeshData.vertices[navMeshData.indices[secondVertexSelected]];


        if ((int) firstVertexPosition.x == (int) secondVertexPosition.x ||
            (int) firstVertexPosition.z == (int) secondVertexPosition.z)
            result = PickRandomPoint();
        else
            result = Vector3.Lerp(firstVertexPosition, secondVertexPosition, Random.Range(0.05f, 0.95f));

        if(points.All(point => Vector3.Distance(point, result) > 1))
        {
            NavMeshHit hit;
            NavMesh.SamplePosition(result, out hit, 2.0f, NavMesh.AllAreas);
            points.Add(hit.position);
            return hit.position;
        }

        return PickRandomPoint();

    }
}
