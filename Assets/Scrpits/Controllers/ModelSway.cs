﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ModelSway : MonoBehaviour
{
    public float movementAmplitudeY = 0.5f;
    public float movementAmplitudeX = 0.5f;

    public float stopAmplitudeY = 0.5f;
    public float stopAmplitudeX = 0f;
    [SerializeField, Range(1f, 10f)] public float transitionSpeed = 4f;
    public bool isMoving;
    
    float currentAmplitudeY;
    float currentAmplitudeX;
    Vector3 posOffset;
    
    void Start() => posOffset = transform.localPosition;

    void Update()
    {
        if(isMoving)
            Sway(movementAmplitudeX, movementAmplitudeY);
        else
            Sway(stopAmplitudeX, stopAmplitudeY);
    }

    void Sway(float targetAmplitudeX, float targetAmplitudeY)
    {
        currentAmplitudeX = Mathf.Lerp(currentAmplitudeX, targetAmplitudeX, Time.deltaTime * transitionSpeed);
        currentAmplitudeY = Mathf.Lerp(currentAmplitudeY, targetAmplitudeY, Time.deltaTime * transitionSpeed);
        
        var tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI) * currentAmplitudeY;
        tempPos.x += Mathf.Sin(Time.fixedTime * Mathf.PI) * currentAmplitudeX;
        transform.localPosition = tempPos;
    }
}
