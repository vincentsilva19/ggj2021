using Scenes.VincentSceneAssets.Scripts.Domain;
using UnityEngine;
using UnityEngine.UI;

namespace Scrpits.UI
{
    public class TimerController : MonoBehaviour
    {
        
        public int timerStep = 1;
        public int totalTime = 30;
        float actualTime;
        
        public GameObject timeBar;
        public Image flame;
        public Image blackView;
        
        public float maxSize;
        public float minSize;
        public GameEvents events;

        void Start()
        {
            actualTime = totalTime;
        }

        void Update()
        {
            if (actualTime > 0)
            {
                UpdateTimer();
                ChangeTimeBarSize();
            }
        }

        void ChangeTimeBarSize()
        {
            var newScale = 1 * (actualTime / totalTime);
            var size = Mathf.Lerp(minSize, maxSize, newScale);
            timeBar.GetComponent<RectTransform>().sizeDelta = new Vector2(timeBar.GetComponent<RectTransform>().sizeDelta.x, size);
            flame.rectTransform.localScale = new Vector3(2f * newScale + 0.2f,  2f * newScale + 0.2f, 1);
            var color = blackView.color;
            color.a = 1f - newScale;
            blackView.color = color;
        }

        void UpdateTimer()
        {
            actualTime -= timerStep * Time.deltaTime;
            if (actualTime <= 0)
                events.TimerEndAction();
        }
    }
}