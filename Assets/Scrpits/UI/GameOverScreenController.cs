﻿using Scenes.VincentSceneAssets.Scripts.Domain;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scenes.VincentSceneAssets.Scripts.UI
{
    public class GameOverScreenController : MonoBehaviour
    {
        public GameEvents events;
        public Button start;
        public GameObject background;
        void Start()
        {
            events.loseGame.Add(ShowScreen);
            start.onClick.AddListener(StartGame);
        }

        void StartGame()
        {
            SceneManager.LoadScene("IntroScene");
            
        }

        void ShowScreen()
        {
            background.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

        }
    }
}
