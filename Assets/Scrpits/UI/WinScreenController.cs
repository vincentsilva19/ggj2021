﻿using Scenes.VincentSceneAssets.Scripts.Domain;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scenes.VincentSceneAssets.Scripts.UI
{
    public class WinScreenController : MonoBehaviour
    {
        public GameEvents events;
        public GameObject background;
        public Button start;
        public Button exit;

        void Start()
        {
            events.winGame.Add(ShowScreen);
            start.onClick.AddListener(StartGame);
            exit.onClick.AddListener(QuitGame);
        }

        void ShowScreen()
        {
            background.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void StartGame() => SceneManager.LoadScene("IntroScene");
        
        void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
             Application.Quit();
#endif
        }
    }
}
