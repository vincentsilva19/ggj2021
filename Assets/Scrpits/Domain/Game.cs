using System;
using UnityEngine;

namespace Scenes.VincentSceneAssets.Scripts
{
    public class Game
    {
        readonly Action showWinScreen;
        readonly Action showGameOverScreen;

        public Game()
        {
            
        }

        public Game(Action showWinScreen, Action showGameOverScreen)
        {
            this.showWinScreen = showWinScreen;
            this.showGameOverScreen = showGameOverScreen;
        }

        public void OnTimerEnd() => showGameOverScreen();

        public void OnCorrectBodyEnter() => showWinScreen();

        public void OnCollectWrongBody()
        {
            Debug.Log("Wrong body");
        }
    }
}