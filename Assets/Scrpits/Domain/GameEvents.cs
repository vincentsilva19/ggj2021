﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scenes.VincentSceneAssets.Scripts.Domain
{
    public class GameEvents : MonoBehaviour
    {
        public readonly List<Action> loseGame = new List<Action>();
        public List<Action> winGame = new List<Action>();
        public AudioSource correct;
        public AudioSource wrong;
        Game game;
        
        void Start() => game = new Game(
            () => winGame.ForEach(action => action()), 
            () => loseGame.ForEach(action => action()));

        void ShowGameOverScreen() => loseGame.ForEach(action => action());

        void ShowWinScreen() => winGame.ForEach(action => action());

        public void TimerEndAction() => game.OnTimerEnd();

        public void CollectBodyCorrectBody()
        {
            game.OnCorrectBodyEnter();
            correct.Play();
        }

        public void CollectWrongBody()
        {
            game.OnCollectWrongBody();
            wrong.Play();
        }
    }
}
